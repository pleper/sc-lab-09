package model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator<Object> {

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		Taxable self = (Taxable) o1;
		Taxable other = (Taxable) o2;

		if (self.getTax() < other.getTax())
			return -1;
		if (self.getTax() == other.getTax())
			return 0;
		return 1;
	}

}
