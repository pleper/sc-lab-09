package model;

import Interface.Taxable;

public class Product implements Taxable ,Comparable {
	
	private String name;
	private double price;
	
	public Product(String aName, double aPrice){
		name = aName;
		price = aPrice;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return (int)(price*(7/100.0));
	}
	public double getPrice(){
		return price;
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Product other = (Product)o;
		if(price < other.getPrice())return -1;
		else if(price > other.getPrice()) return 1;
		return 0;
	}
	public String toString(){
		return name;
	}

}
