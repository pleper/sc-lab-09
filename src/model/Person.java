package model;

import Interface.Measurable;
import Interface.Taxable;

public class Person implements Measurable , Taxable ,Comparable<Object>{
	
	private String name;
	private double high;
	private double salary;
	
	public Person(String aName, double aHigh){
		name = aName;
		high = aHigh;
		salary = aHigh;
	}
	
	public String getName(){
		return name;
	}
	
	public double getHigh(){
		return high;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return high;
	}

	@Override
	public double getTax() {
		double n = 0;
		// TODO Auto-generated method stub
		if (salary <= 300000){
			return (int)(salary*(5/100));
		}
		n = salary - 300000;
		return (int)((300000*(5/100.0)) + (n*(10/100.0)));
	
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Person other = (Person)o;
		if(salary < other.getMeasure())return -1;
		else if(salary > other.getMeasure()) return 1;
		return 0;
	}
	public String toString(){
		return name;
	}
}
