package model;

import java.util.Comparator;

public class EarningComparator implements Comparator<Object> {
	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		Company self = (Company) o1;
		Company other = (Company) o2;

		if (self.getRe() < other.getRe())
			return -1;
		if (self.getRe() == other.getRe())
			return 0;
		return 1;
	}
}
