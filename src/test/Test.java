package test;

import java.util.ArrayList;
import java.util.Collections;

import Interface.Taxable;
import model.Company;
import model.EarningComparator;
import model.ExpenseComparator;
import model.Person;
import model.Product;
import model.ProfitComparator;
import model.TaxComparator;

public class Test {
	public static void main(String[] args){
		Test t =new Test();
		t.testPerson();
		t.testProduct();
		t.testCompany();
		t.testTaxComparable();
	}
	public void testPerson(){
		ArrayList<Person> p = new ArrayList<>();
		p.add(new Person("A",1000));
		p.add(new Person("B",3000));
		p.add(new Person("C",2000));
		p.add(new Person("D",500));
		System.out.println("TestPerson :");
		System.out.println("Before Sort : " + p);
		Collections.sort(p);
		System.out.println("After sort : "+p);
	}
	public void testProduct(){
		ArrayList<Product> pr = new ArrayList<>();
		pr.add(new Product("A1",3000));
		pr.add(new Product("B1",1000));
		pr.add(new Product("C1",2500));
		pr.add(new Product("D1",2300));
		System.out.println("TestPerson :");
		System.out.println("Before Sort : " + pr);
		Collections.sort(pr);
		System.out.println("After sort : "+pr);
	}
	public void testCompany(){
		EarningComparator comEarn = new EarningComparator();
		ExpenseComparator comEx = new ExpenseComparator();
		ProfitComparator comPro = new ProfitComparator();
		
		ArrayList<Company> c = new ArrayList<>();
		c.add(new Company("one ",9000,100));
		c.add(new Company("two",5000,500));
		c.add(new Company("three",10000,5000));
		c.add(new Company("four",9500,150));
		System.out.println("Test Company :");
		System.out.println("Test Earning comparator :");
		System.out.println("Before Sort : " + c);
		Collections.sort(c,comEarn);
		System.out.println("After sort : "+c);
		System.out.println("Test Expense comparator :");
		Collections.sort(c,comEx);
		System.out.println("After sort : "+c);
		System.out.println("Test Profit comparator :");
		Collections.sort(c,comPro);
		System.out.println("After sort : "+c);
	}
	public void testTaxComparable(){
		TaxComparator comTax = new TaxComparator();
		ArrayList<Taxable> a = new ArrayList<>();
		a.add(new Person("tutu",500));
		a.add(new Person("tata",6000));
		a.add(new Product("Phone",99999));
		a.add(new Product("Paper",1000));
		a.add(new Company("Jai", 500000, 25000));
		a.add(new Company("Kai", 4000, 70));
		System.out.println("Test TaxCompare :");
		System.out.println("Before Sort : " + a);
		Collections.sort(a,comTax);
		System.out.println("After sort : "+a);
	}
}
